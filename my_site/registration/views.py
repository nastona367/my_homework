from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse
from .models import *


def reg(request):
    name = request.POST['name']
    return HttpResponse(f'{name}, ваша регестрація успішна!')

def new_reg(request):
    return render(request, 'pages/newregistration.html')
