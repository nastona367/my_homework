from django.urls import path
from .views import *

urlpatterns = [
    path('reg/', reg, name='reg'),
    path('', new_reg, name='new_reg')]
