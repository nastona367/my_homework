from django.urls import path
from .views import *

urlpatterns = [
    path('', new_reg, name='new_reg'),
    path('new_comment/', new_comment, name='new_comment')
]
